import os
import metadata_helper as mdh

traverse_this = '/lustre03/project/6007512/yatharth/mugqic/'

def generate_all_jsons():
    all_soft = os.listdir(traverse_this)
    for soft in all_soft:
        mdh.run_mdh(soft)

if __name__ == '__main__':
	generate_all_jsons()
